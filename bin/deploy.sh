echo "========================";
echo "Starting deploy"
echo "========================";
touch ./deploy.running

echo "Loading data from GIT";
echo "========================";
# pull from current branch
git pull
echo "========================";

echo "Installing composer dependencies";
echo "========================";
# install composer
composer install --no-dev
echo "========================";

echo "Removing temporary folder"
echo "========================";
## FPM-FCGI
rm -r ./temp/cache/Nette.RobotLoader
rm -r ./temp/cache/Nette.Configurator
rm -r ./temp/cache/latte
rm -r ./www/img/gallery
rm -r ./www/img/thumbnails
echo "========================";

echo "Doing changes in database";
echo "========================";
mkdir -p ./migrations/structures
mkdir -p ./migrations/dummy-data
mkdir -p ./migrations/basic-data

#php7.2 ./bin/console migrations:continue
echo "========================";

rm ./deploy.running
echo "Deploy finished";
echo "========================";