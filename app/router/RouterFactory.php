<?php

declare(strict_types = 1);

namespace App;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

/**
 * Router factory.
 */
class RouterFactory
{

	public function createRouter(): RouteList
	{
		$router = new RouteList();
		$router[] = new Route('[<locale=cs cs|en|sk>/]portfolio/<id>/', 'Front:Portfolio:view');

		// routelist pro modul admin
		$router[] = $adminRoutes = new RouteList('Admin');
		$adminRoutes[] = new Route('admin/<presenter>/<action>/[<id>/]', 'Homepage:default');

		// route list pro modul front
		$router[] = $frontRoutes = new RouteList('Front');
		$frontRoutes[] = new Route('[<locale=cs cs|en|sk>/]<presenter>/<action>/[<id>]', 'Homepage:default');

		return $router;
	}

}
