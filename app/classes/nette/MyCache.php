<?php

declare(strict_types = 1);

namespace App\Classes\Nette;

/**
 * Overrided cache class from Nette framework
 */
class MyCache extends \Nette\Caching\Cache
{

	/**
	 * Load from cache, if offset not exist insert it to cache and then return
	 * @param string|mixed $name
	 * @param \callback|mixed $fallback
	 * @param mixed $parameters
	 * @return mixed
	 */
	public function load($name, $fallback = null, $parameters = null)
	{
		$key = self::getNamesForCache($name, $parameters);
		return parent::load($key, $fallback);
	}

	/**
	 * Save data to cache
	 * @param string|mixed $name
	 * @param string|mixed $data
	 * @param mixed[] $dependencies
	 * @param mixed $parameters
	 * @return mixed
	 */
	public function save($name, $data, ?array $dependencies = null, $parameters = null)
	{
		$key = self::getNamesForCache($name, $parameters);
		return parent::save($key, $data, $dependencies);
	}

	/**
	 * Cleans cache by tags
	 * @param string|mixed[]|null $tags
	 */
	final public function cleanByTags($tags = null): void
	{
		if (!isset($tags)) {
			$this->clean([
				self::ALL => true,
			]);
		} else {
			$this->clean([
				self::TAGS => (is_array($tags)) ? $tags : [$tags],
			]);
		}
	}

	/**
	 * Clean cache by priority
	 * @param int $priority
	 */
	final public function cleanByPriority(int $priority): void
	{
		$this->clean([
			self::PRIORITY => $priority,
		]);
	}

	/**
	 * Generate unique cache name by attributes
	 * @param string $methodname
	 * @param mixed[]|string $parameters
	 * @return string
	 * @static
	 */
	public static function getNamesForCache(string $methodname, $parameters = null): string
	{
		if (!$parameters) {
			return $methodname;
		}

		if (is_array($parameters)) {
			foreach ($parameters as $value) {
				if ($value) {
					if (is_array($value)) {
						$methodname .= '_' . implode('_', $value);
					} else {
						$methodname .= '_' . $value;
					}
				}
			}
		} else {
			$methodname .= '_' . $parameters;
		}
		return $methodname;
	}

}
