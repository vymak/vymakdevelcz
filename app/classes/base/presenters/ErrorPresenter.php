<?php

declare(strict_types = 1);

namespace App\Presenters;

/**
 * Base Error presenter
 */
class ErrorPresenter extends \Nette\Application\UI\Presenter
{

	/**
	 * @param \Throwable $exception
	 * @param mixed      $request
	 */
	public function renderDefault(\Throwable $exception, $request): void
	{
		$this->forward(
			':Front:Error:',
			[
				'exception' => $exception,
				'request' => $request,
			]
		);
	}

}
