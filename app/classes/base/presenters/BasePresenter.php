<?php

declare(strict_types = 1);

namespace App\Presenters;

use Tracy\Debugger;

/**
 * Description of BasePresenter
 */
class BasePresenter extends \Nette\Application\UI\Presenter
{

	private const PUBLIC_AVAILABLE = true;

	protected function startup(): void
	{
		parent::startup();
		if (Debugger::$productionMode && !self::PUBLIC_AVAILABLE) {
			$this->template->setFile(__DIR__ . '/../../../FrontModule/templates/Error/503.latte');
		}
	}

	public function afterRender(): void
	{
		if ($this->isAjax() && $this->hasFlashSession()) {
			$this->redrawControl('flashes');
		}
	}

}
