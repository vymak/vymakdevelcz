<?php

declare(strict_types = 1);

namespace App\Classes\Others;

use Nette\DI\Container;
use Nette\Http\Request;

/**
 * Config class return parameters from parameters.neon file
 */
class Config
{

	use \Nette\SmartObject;

	/** @var mixed[] */
	private $parameters;

	/** @var \Nette\Http\Request */
	private $request;

	public function __construct(Container $container, Request $request)
	{
		$this->parameters = $container->getParameters();
		$this->request = $request;
	}

	/**
	 * @return mixed[]
	 */
	public function getParameters(): array
	{
		return $this->parameters;
	}

	public function getRequest(): Request
	{
		return $this->request;
	}

}
