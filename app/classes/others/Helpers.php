<?php

declare(strict_types = 1);

namespace App\Classes;

use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Strings;

/**
 * Helper class
 */
class Helpers
{

	use \Nette\SmartObject;

	/**
	 * Function which register helpers to template
	 *
	 * @param string $filter
	 * @param mixed  $value
	 *
	 * @return mixed
	 * @static
	 */
	public static function common(string $filter, $value)
	{
		if (method_exists(__CLASS__, $filter)) {
			$args = func_get_args();
			array_shift($args);

			return call_user_func_array([__CLASS__, $filter], $args);
		}

		return null;
	}

	/**
	 * Return time ago in word
	 *
	 * @param mixed $time
	 *
	 * @return null|string
	 * @static
	 */
	public static function timeAgoInWords($time): ?string
	{
		if (!$time) {
			return null;
		} elseif (is_numeric($time)) {
			$time = (int) $time;
		} elseif ($time instanceof DateTime) {
			$time = $time->format('U');
		} else {
			$time = strtotime($time);
		}

		$delta = time() - $time;

		if ($delta < 0) {
			$delta = round(abs($delta) / 60);
			if ($delta === 0) {
				return 'za okamžik';
			}
			if ($delta === 1) {
				return 'za minutu';
			}
			if ($delta < 45) {
				return 'za ' . $delta . ' ' . self::plural($delta, 'minuta', 'minuty', 'minut');
			}
			if ($delta < 90) {
				return 'za hodinu';
			}
			if ($delta < 1440) {
				return 'za ' . round($delta / 60) . ' ' . self::plural(round($delta / 60), 'hodina', 'hodiny', 'hodin');
			}
			if ($delta < 2880) {
				return 'zítra';
			}
			if ($delta < 43200) {
				return 'za ' . round($delta / 1440) . ' ' . self::plural(round($delta / 1440), 'den', 'dny', 'dní');
			}
			if ($delta < 86400) {
				return 'za měsíc';
			}
			if ($delta < 525960) {
				return 'za ' . round($delta / 43200) . ' ' . self::plural(
					round($delta / 43200),
					'měsíc',
					'měsíce',
					'měsíců'
				);
			}
			if ($delta < 1051920) {
				return 'za rok';
			}

			return 'za ' . round($delta / 525960) . ' ' . self::plural(round($delta / 525960), 'rok', 'roky', 'let');
		}

		$delta = round($delta / 60);
		if ($delta === 0) {
			return 'před okamžikem';
		}
		if ($delta === 1) {
			return 'před minutou';
		}
		if ($delta < 45) {
			return sprintf('před %s minutami', $delta);
		}
		if ($delta < 90) {
			return 'před hodinou';
		}
		if ($delta < 1440) {
			return 'před ' . round($delta / 60) . ' hodinami';
		}
		if ($delta < 2880) {
			return 'včera';
		}
		if ($delta < 43200) {
			return 'před ' . round($delta / 1440) . ' dny';
		}
		if ($delta < 86400) {
			return 'před měsícem';
		}
		if ($delta < 525960) {
			return 'před ' . round($delta / 43200) . ' měsíci';
		}
		if ($delta < 1051920) {
			return 'před rokem';
		}

		return 'před ' . round($delta / 525960) . ' lety';
	}

	/**
	 * Plural: three forms, special cases for 1 and 2, 3, 4.
	 * (Slavic family: Slovak, Czech)
	 *
	 * @param int|float $n
	 *
	 * @return mixed
	 * @static
	 */
	private static function plural($n)
	{
		$args = func_get_args();

		return $args[($n === 1) ? 1 : (($n >= 2 && $n <= 4) ? 2 : 3)];
	}

	/**
	 * Smarty {mailto} function plugin
	 *
	 * @param string $email
	 *
	 * @link http://www.smarty.net/manual/en/language.function.mailto.php {mailto}
	 * (Smarty online manual)
	 * @static
	 * @return string
	 */
	public static function emailProtection(string $email): string
	{
		$address = $email;

		$match = [];
		preg_match('!^(.*)(\?.*)$!', $address, $match);
		if (!$match[2]) {
			trigger_error('mailto: hex encoding does not work with extra attributes. Try javascript.', E_USER_WARNING);

			return '';
		}
		$addressEncode = '';
		for ($x = 0, $length = strlen($address); $x < $length; $x++) {
			if (preg_match('!\w!u', $address[$x])) {
				$addressEncode .= '%' . bin2hex($address[$x]);
			} else {
				$addressEncode .= $address[$x];
			}
		}
		$mailto = '&#109;&#97;&#105;&#108;&#116;&#111;&#58;';

		return '<a href="' . $mailto . $addressEncode . '">' . $address . '</a>';
	}

	public static function image(string $img, ?int $width = null, ?int $height = null, int $mode = Image::EXACT): string
	{
		$name = Strings::webalize(hash('SHA256', $img));

		$path = $name . '.jpg';
		$rootDir = __DIR__ . '/../../../www/';
		$dir = $rootDir . 'img/gallery/';

		if (!is_dir($dir)) {
			FileSystem::createDir($dir);
		}

		if (!file_exists($dir . $path)) {
			$imageObj = Image::fromString($img);

			// volitelné zmenšení obrázku
			if ($width && $height) {
				$imageObj->resize($width, $height, $mode);
			} elseif ($width) {
				$imageObj->resize($width, null);
			} elseif ($height) {
				$imageObj->resize(null, $height);
			}

			$imageObj->save($dir . $path, null, Image::JPEG);
		}

		return $name;
	}

	/**
	 * @param mixed    $image
	 * @param string   $name
	 * @param int|null $width
	 * @param int|null $height
	 *
	 * @return string
	 */
	public static function thumbnails($image, string $name, ?int $width = null, ?int $height = null): string
	{
		$tmp = Strings::truncate(Strings::trim($name), 50);
		$dir = __DIR__ . '/../../../www/img/thumbnails/';

		$suffix = ($width && $height) ? '-' . $width . '-' . $height : null;
		$name = Strings::webalize($tmp) . $suffix . '.jpg';

		if (!is_dir($dir)) {
			FileSystem::createDir($dir);
		}

		if (file_exists($dir . $name)) {
			return $name;
		}

		if (Strings::match($image, '/^.*.(png|jpg|gif|jpeg)$/')) {
			if (file_exists($image)) {
				$image = Image::fromFile($image);
			} else {
				trigger_error('Nemůžu načíst soubor s obrázkem', E_USER_ERROR);
			}
		} else {
			try {
				$image = Image::fromString($image);
			} catch (\Nette\Utils\ImageException $exc) {
				// todo
				//              trigger_error('String pro načtení obrázku není obrázek', E_USER_ERROR);
				return '';
			}
		}

		if ($width && $height) {
			if ($width > $image->width || $height > $image->height) {
				$blankImg = Image::fromBlank(
					$width,
					$height,
					[
						'red' => 255,
						'green' => 255,
						'blue' => 255,
						'alpha' => 0,
					]
				);
				$blankImg->place($image, (int) (($width - $image->width) / 2), (int) (($height - $image->height) / 2));
				$image = $blankImg;
			} else {
				$image->resize($width, $height, Image::EXACT);
			}
		}

		$image->save($dir . $name);

		return $name;
	}

}
