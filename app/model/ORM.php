<?php

declare(strict_types = 1);

namespace App\Model;

/**
 * @property-read ExchangeLinkRepository        $exchangeLink
 * @property-read PortfolioRepository           $portfolio
 * @property-read TechnologyRepository          $technology
 * @property-read PortfolioTechnologyRepository $portfolioTechnology
 * @property-read PortfolioImageRepository      $portfolioImage
 */
class ORM extends \Nextras\Orm\Model\Model
{

}
