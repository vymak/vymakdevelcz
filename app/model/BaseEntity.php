<?php

declare(strict_types = 1);

namespace App\Model;

use Nette\Utils\ArrayHash;
use Nextras\Orm\Entity\IEntity;

/**
 * @property-read string $entityHashCode {virtual}
 */
abstract class BaseEntity extends \Nextras\Orm\Entity\Entity
{

	/** @var string */
	private $salt = 'Bpaf_7edOBI6M4AqVJ76E14XIhZG2gmwUm_hI1XN';

	public static function from(ArrayHash $data): IEntity
	{
		$obj = new static();
		foreach ($data as $key => $value) {
			if ($key !== 'id') {
				$obj->$key = $value;
			}
		}

		/**
		 * @var string           $name
		 * @var \Nextras\Orm\Entity\Reflection\PropertyMetadata $property
		 */
		foreach ($obj->getMetadata()->getProperties() as $name => $property) {
			if (!isset($obj->$name) && $name !== 'id' && !$property->isReadonly) {
				$val = null;
				switch (array_keys($property->types)[0]) {
					case 'int':
						$val = (int) $property->defaultValue;
						break;
					default:
						$val = $property->defaultValue;
						break;
				}
				$obj->$name = $val;
			}
		}

		return $obj;
	}

	public function getterEntityHashCode(): string
	{
		$hash = $this->salt . get_called_class();
		foreach ($this->getMetadata()->getProperties() as $property) {
			if (isset($this->{$property->name}) && !$property->isReadonly && !$property->isVirtual && in_array(array_keys($property->types)[0], ['string', 'int'])) {
				$hash = hash('SHA512', $hash . $this->{$property->name}, false);
			}
		}

		return $hash;
	}

}
