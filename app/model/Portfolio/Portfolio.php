<?php

declare(strict_types = 1);

namespace App\Model;

use Nextras\Dbal\Utils\DateTimeImmutable;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property int                                   $id {primary}
 * @property string                                $name
 * @property string                                $title
 * @property string                                $description
 * @property string                                $type {enum self::TYPE_*}
 * @property DateTimeImmutable                     $date
 * @property string|NULL                           $link
 *
 * @property-read OneHasMany|PortfolioTechnology[] $portfolioTechnologies {1:m PortfolioTechnology::$portfolioId} {virtual}
 * @property-read OneHasMany|PortfolioImage[]      $portfolioImages {1:m PortfolioImage::$portfolioId} {virtual}
 * @property-read PortfolioImage|null $mainImage
 * @property-read string $technologyString
 */
final class Portfolio extends \App\Model\BaseEntity
{

	public const TYPE_WEBDESIGN = 'webdesign',
		TYPE_OTHER = 'other';

	public function getterMainImage(): ?PortfolioImage
	{
		foreach ($this->portfolioImages as $image) {
			if ($image->main) {
				return $image;
			}
		}

		return null;
	}

	public function getterTechnologyString(): string
	{
		$arr = [];
		foreach ($this->portfolioTechnologies as $technology) {
			$arr[] = $technology->technologyId->name;
		}
		return implode(' - ', $arr);
	}

}
