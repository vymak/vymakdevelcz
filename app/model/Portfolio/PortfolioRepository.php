<?php

declare(strict_types = 1);

namespace App\Model;

final class PortfolioRepository extends \App\Model\BaseRepository
{

	/**
	 * @return \Nextras\Orm\Collection\ICollection|\App\Model\Portfolio[]
	 */
	public function findMainPortfolio()
	{
		return $this->findBy(['this->portfolioImages->main' => 1]);
	}

	/**
	 * Returns possible entity class names for current repository.
	 * @return string[]
	 */
	public static function getEntityClassNames(): array
	{
		return [Portfolio::class];
	}

}
