<?php

declare(strict_types = 1);

namespace App\Model;

final class TechnologyRepository extends \App\Model\BaseRepository
{

	/**
	 * Returns possible entity class names for current repository.
	 * @return string[]
	 */
	public static function getEntityClassNames(): array
	{
		return [Technology::class];
	}

}
