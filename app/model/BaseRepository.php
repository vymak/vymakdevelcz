<?php

declare(strict_types = 1);

namespace App\Model;

use Nette\Utils\ArrayHash;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;

abstract class BaseRepository extends \Nextras\Orm\Repository\Repository
{

	public function update(int $id, ArrayHash $arr): IEntity
	{
		$entity = $this->getById($id);
		foreach ($arr as $name => $value) {
			if ($name !== 'id') {
				$entity->setValue($name, $value);
			}
		}

		return $this->persistAndFlush($entity);
	}

	/**
	 * @param \Nextras\Orm\Entity\IEntity[] $entities
	 *
	 * @return mixed[]
	 */
	public function updateEntities(array $entities): array
	{
		foreach ($entities as $entity) {
			if ($entity instanceof IEntity) {
				$this->persist($entity);
			}
		}

		return $this->doFlush();
	}

	/**
	 * @param string $column
	 *
	 * @return mixed[]
	 */
	final public function findDistinct(string $column): array
	{
		return $this->findDistinctFromCollection($column, $this->findAll());
	}

	/**
	 * @param  string                             $column
	 * @param \Nextras\Orm\Collection\ICollection $collection
	 *
	 * @return mixed[]
	 */
	final public function findDistinctFromCollection(string $column, ICollection $collection): array
	{
		$returnArr = [];

		foreach ($collection as $item) {
			if (!array_key_exists($item->$column, $returnArr)) {
				$returnArr[$item->$column] = $item;
			}
		}

		return $returnArr;
	}

}
