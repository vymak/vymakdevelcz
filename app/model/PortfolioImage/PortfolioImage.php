<?php

declare(strict_types = 1);

namespace App\Model;

/**
 * @property int         $id {primary}
 * @property Portfolio   $portfolioId {m:1 Portfolio::$portfolioImages}
 * @property string      $image
 * @property int         $main {default 0}
 *
 * @property-read string $imageBase64Encode {virtual}
 */
final class PortfolioImage extends \App\Model\BaseEntity
{

	public function getterImageBase64Encode(): string
	{
		return base64_encode($this->image);
	}

}
