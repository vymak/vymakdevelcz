<?php

declare(strict_types = 1);

namespace App\Model;

/**
 * @property int        $id {primary}
 * @property Portfolio  $portfolioId {m:1 Portfolio::$portfolioTechnologies}
 * @property Technology $technologyId {m:1 Technology, oneSided=true}
 */
final class PortfolioTechnology extends \App\Model\BaseEntity
{

}
