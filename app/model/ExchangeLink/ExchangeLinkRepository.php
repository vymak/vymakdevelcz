<?php

declare(strict_types = 1);

namespace App\Model;

use Nextras\Orm\Collection\ICollection;

final class ExchangeLinkRepository extends \App\Model\BaseRepository
{

	/**
	 * @return \Nextras\Orm\Collection\ICollection|\App\Model\ExchangeLink[]
	 */
	public function findAllActive(): ICollection
	{
		return $this->findBy(['active' => 1]);
	}

	/**
	 * @param string|string[] $category
	 * @return \Nextras\Orm\Collection\ICollection|\App\Model\ExchangeLink[]
	 */
	public function findAllActiveByCategory($category): ICollection
	{
		return $this->findBy(['active' => 1, 'category' => $category]);
	}

	/**
	 * Returns possible entity class names for current repository.
	 * @return string[]
	 */
	public static function getEntityClassNames(): array
	{
		return [ExchangeLink::class];
	}

}
