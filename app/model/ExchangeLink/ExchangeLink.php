<?php

declare(strict_types = 1);

namespace App\Model;

/**
 * Setting
 * @property int    $id {primary}
 * @property string $web
 * @property string $title
 * @property int    $active
 * @property string $category {enum self::TYPE_*}
 */
final class ExchangeLink extends \App\Model\BaseEntity
{

	public const TYPE_ALL = 'all',
		TYPE_MAIN = 'main',
		TYPE_PARTNERS = 'partners';

}
