<?php

declare(strict_types = 1);

namespace App\FrontModule\Model;

use App\Model\ORM;
use Nette\Security\Identity;
use Nette\Utils\Random;
use Nette\Utils\Strings;

/**
 * Users management.
 */
class UserManager implements \Nette\Security\IAuthenticator
{

	use \Nette\SmartObject;

	private const TABLE_NAME = 'users',
		COLUMN_ID = 'id',
		COLUMN_NAME = 'username',
		COLUMN_PASSWORD = 'password',
		COLUMN_ROLE = 'role',
		PASSWORD_MAX_LENGTH = 4096;

	/** @var \App\Model\ORM */
	private $orm;

	public function __construct(ORM $orm)
	{
		$this->orm = $orm;
	}

	/**
	 * Performs an authentication.
	 * @param mixed[] $credentials
	 * @return \Nette\Security\Identity
	 */
	public function authenticate(array $credentials): Identity
	{
		[$username, $password] = $credentials;
		$row = $this->orm->table(self::TABLE_NAME)->where(self::COLUMN_NAME, $username)->fetch();

		if (!$row) {
			throw new \Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
		} elseif (!self::verifyPassword($password, $row[self::COLUMN_PASSWORD])) {
			throw new \Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
		} elseif (PHP_VERSION_ID >= 50307 && substr($row[self::COLUMN_PASSWORD], 0, 3) === '$2a') {
			$row->update([
				self::COLUMN_PASSWORD => self::hashPassword($password),
			]);
		}

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD]);
		return new Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}

	/**
	 * Adds new user.
	 * @param  string $username
	 * @param  string $password
	 * @return void
	 */
	public function add(string $username, string $password): void
	{
		$this->orm->table(self::TABLE_NAME)->insert([
			self::COLUMN_NAME => $username,
			self::COLUMN_PASSWORD => self::hashPassword($password),
		]);
	}

	/**
	 * Computes salted password hash.
	 * @param  string $password
	 * @param  mixed[]|null $options
	 * @return string
	 */
	public static function hashPassword(string $password, ?array $options = null): string
	{
		if ($password === Strings::upper($password)) { // perhaps caps lock is on
			$password = Strings::lower($password);
		}
		$password = substr($password, 0, self::PASSWORD_MAX_LENGTH);
		$options = $options ? : implode('$', [
			'algo' => PHP_VERSION_ID < 50307 ? '$2a' : '$2y', // blowfish
			'cost' => '07',
			'salt' => Random::generate(22),
		]);
		return crypt($password, $options);
	}

	/**
	 * Verifies that a password matches a hash.
	 * @param string $password
	 * @param string $hash
	 * @return bool
	 */
	public static function verifyPassword(string $password, string $hash): bool
	{
		return self::hashPassword($password, $hash) === $hash || (PHP_VERSION_ID >= 50307 && substr($hash, 0, 3) === '$2a' && self::hashPassword($password, $tmp = '$2x' . substr($hash, 3)) === $tmp);
	}

}
