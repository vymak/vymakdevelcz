<?php

declare(strict_types = 1);

namespace App\FrontModule\Control\Factory;

use App\FrontModule\Control\PortfolioListControl;

interface IPortfolioListControl
{

	public function create(): PortfolioListControl;

}
