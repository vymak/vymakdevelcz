<?php

declare(strict_types = 1);

namespace App\FrontModule\Control;

use App\Model\ORM;
use Nette\Application\UI\ITemplate;

class PortfolioListControl extends \App\Control\BaseControl
{

	/** @var \App\Model\PortfolioRepository */
	private $portfolioRepository;

	public function __construct(ORM $orm)
	{
		parent::__construct();
		$this->portfolioRepository = $orm->portfolio;
	}

	protected function createTemplate(): ITemplate
	{
		$template = parent::createTemplate();
		$template->portfolio = $this->portfolioRepository->findMainPortfolio();
		return $template;
	}

}
