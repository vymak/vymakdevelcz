<?php

declare(strict_types = 1);

namespace App\FrontModule\Control\Factory;

use App\FrontModule\Control\ContactFormControl;

interface IContactFormControl
{

	public function create(): ContactFormControl;

}
