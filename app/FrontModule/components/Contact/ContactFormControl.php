<?php

declare(strict_types = 1);

namespace App\FrontModule\Control;

use Nette\Application\UI\Form;
use Nette\Http\Request;
use Nette\Mail\IMailer;
use Nette\Mail\Message;

class ContactFormControl extends \App\Control\BaseControl
{

	/** @var \Nette\Mail\IMailer */
	private $mailer;

	/** @var \Nette\Http\Request */
	private $request;

	public function __construct(IMailer $mailer, Request $request)
	{
		parent::__construct();
		$this->mailer = $mailer;
		$this->request = $request;
	}

	protected function createComponentContactForm(): Form
	{
		$form = new Form();
		$form->getElementPrototype()->class = 'ajax form';
		$form->getElementPrototype()->id = 'form';
		$form->addText('name', 'Jméno')
			->setRequired('Jméno musí být vyplněno!')
			->setAttribute('placeholder', 'Vaše jméno');
		$form->addText('email', 'Email')
			->setType('text')
			->setRequired('Email musí být vyplněn!')
			->addRule($form::EMAIL, 'Zadaná emailová adresa nemá platný formát')
			->setAttribute('placeholder', 'Váš email');
		$form->addTextArea('message', 'Zpráva', null, 8)
			->setRequired('Zpráva musí být vyplněna!')
			->setAttribute('placeholder', 'Zpráva');
		$form->addSubmit('send', 'Odeslat')
			->setAttribute('class', 'btn mt-button medium outline');

		$form->onSuccess[] = [$this, 'onSuccess'];
		return $form;
	}

	public function onSuccess(Form $form): void
	{
		$values = $form->values;

		$message = new Message();
		$message->addTo('l.vymetalik@vymakdevel.cz', 'Ing. Libor Vymětalík')
			->setFrom($values->email, $values->name)
			->setBody($values->message)
			->setSubject('Zpráva z kontaktního formuláře');

		$this->mailer->send($message);
		$this->flashMessage('Zpráva byla úspěšně odeslána. V brzké době Vás budu kontaktovat', true);

		if ($this->request->ajax) {
			$this->redrawControl('form');
			$form->setValues([
				'name' => null,
				'email' => null,
				'message' => null,
			], false);
		} else {
			$this->redirect('this');
		}
	}

}
