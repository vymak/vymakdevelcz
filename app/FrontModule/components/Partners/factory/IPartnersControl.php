<?php

declare(strict_types = 1);

namespace App\FrontModule\Control\Factory;

use App\FrontModule\Control\PartnersControl;

interface IPartnersControl
{

	public function create(): PartnersControl;

}
