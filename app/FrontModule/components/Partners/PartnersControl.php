<?php

declare(strict_types = 1);

namespace App\FrontModule\Control;

use App\Model\ORM;

class PartnersControl extends \App\Control\BaseControl
{

	/** @var \App\Model\ExchangeLinkRepository */
	private $exchangeLinkRepository;

	public function __construct(ORM $orm)
	{
		parent::__construct();
		$this->exchangeLinkRepository = $orm->exchangeLink;
	}

	public function render(): void
	{
		$category = ($this->presenter->name === 'Front:Homepage') ? ['main', 'all'] : 'all';

		$template = $this->template;
		$template->setFile($this->getTemplateFile(__FUNCTION__));
		$template->partners = $this->exchangeLinkRepository->findAllActiveByCategory($category);
		parent::render();
	}

	public function renderExchangePage(): void
	{
		$template = $this->template;
		$template->setFile($this->getTemplateFile(__FUNCTION__));
		$template->partners = $this->exchangeLinkRepository->findAllActiveByCategory('partners');
		parent::render();
	}

}
