<?php

declare(strict_types = 1);

namespace App\FrontModule\Control;

class BlogListControl extends \App\Control\BaseControl
{

	public function render(): void
	{
		$isHomepage = ($this->presenter->name === 'Front:Homepage') ? true : false;
		$this->template->partners = $this->partners->getPartnersFooter($isHomepage);
		parent::render();
	}

	public function renderFooter(): void
	{
		$template = $this->template;
		$template->setFile($this->getTemplateFile(__FUNCTION__));

		$template->partners = $this->partners->getPartnersPage();
		parent::render();
	}

}
