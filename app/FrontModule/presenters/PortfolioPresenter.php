<?php

declare(strict_types = 1);

namespace App\FrontModule\Presenters;

class PortfolioPresenter extends \App\FrontModule\Presenters\BaseFrontPresenter
{

	/** @var \App\Model\ORM @inject */
	public $orm;

	public function actionView(string $id): void
	{
		/** @var \App\Model\Portfolio $model */
		$model = $this->orm->portfolio->getBy(['name' => $id]);
		if (!$model) {
			throw new \Nette\Application\BadRequestException();
		}
		$this->template->model = $model;
	}

}
