<?php

declare(strict_types = 1);

namespace App\FrontModule\Presenters;

use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

/**
 * Base presenter for all application presenters.
 */
abstract class BaseFrontPresenter extends \App\Presenters\BasePresenter
{

	use \Vymakdevel\NetteBase\Traits\TBaseControlPresenter;

	/** @var \WebLoader\Nette\LoaderFactory */
	private $webloader;

	/** @var \App\FrontModule\Control\Factory\IContactFormControl @inject */
	public $componentsContactForm;

	/** @var \App\FrontModule\Control\Factory\IPartnersControl @inject */
	public $componentsPartners;

	public function injectWebloader(LoaderFactory $factory): void
	{
		$this->webloader = $factory;
	}

	public function createComponentJsLibs(): JavaScriptLoader
	{
		return $this->webloader->createJavaScriptLoader('libs');
	}

	public function createComponentJsNetteFiles(): JavaScriptLoader
	{
		return $this->webloader->createJavaScriptLoader('netteFiles');
	}

	public function createComponentCssMain(): CssLoader
	{
		return $this->webloader->createCssLoader('main');
	}

}
