<?php

declare(strict_types = 1);

namespace App\FrontModule\Presenters;

use Nette\Utils\DateTime;

class HomepagePresenter extends \App\FrontModule\Presenters\BaseFrontPresenter
{

	/** @var \App\FrontModule\Control\Factory\IPortfolioListControl @inject */
	public $componentsPortfolioList;

	public function renderDefault(): void
	{
		$date = DateTime::from('04/07/1990');
		$diff = (date_diff($date, new DateTime(), true));
		$this->template->age = $diff->y;
	}

}
