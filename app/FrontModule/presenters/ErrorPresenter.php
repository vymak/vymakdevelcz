<?php

declare(strict_types = 1);

namespace App\FrontModule\Presenters;

use Tracy\Debugger;

/**
 * Error presenter.
 */
class ErrorPresenter extends \App\FrontModule\Presenters\BaseFrontPresenter
{

	public function renderDefault(\Throwable $exception): void
	{
		if ($this->isAjax()) { // AJAX request? Just note this error in payload.
			$this->payload->error = true;
			$this->terminate();
		} elseif ($exception instanceof \Nette\Application\BadRequestException) {
			$code = $exception->getCode();
			// load template 403.latte or 404.latte or ... 4xx.latte
			$this->setView(in_array($code, [403, 404, 405, 410, 500]) ? $code : '4xx');
			// log to access.log
			Debugger::log(sprintf('HTTP code %s: %s in %s:%s', $code, $exception->getMessage(), $exception->getFile(), $exception->getLine()), 'access');
		} else {
			$this->setView('500'); // load template 500.latte
			Debugger::log($exception, Debugger::ERROR); // and log exception
		}
	}

}
