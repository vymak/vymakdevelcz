$(function () {
    $.nette.init();
});

Nette.addError = function (elem, message) {
    if (elem.focus)
        elem.focus();

    if (message) {
        $(elem.parentNode).closest("form").find("span.error").remove();
        $(elem.parentNode).closest("form").find("span.form-error-message").remove();
        $(elem).before('<span class="form-error-message">' + message + '</span>');
    }
};